using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

/// <summary>
/// ��� ������ � Json.
/// </summary>
using Newtonsoft.Json;
/// <summary>
/// ��� �������� ������ �� ������ �������������.
/// </summary>
using Fleck;
/// <summary>
/// �����������.
/// </summary>
using NLog;
using MyWebApi.Models;

namespace MyWebApi.Controllers
{
    [Route("api/[controller]")]
    public class SampleDataController : Controller
    {
        private static string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        /// <summary>
        /// �������� ���������
        /// </summary>
        /// <param name = "WebApi"></param>
        [HttpPost]
        public string SendMessage([FromForm] string request)
        {
            var DataMessage = request.FromJson<DataMessage>();
            if (string.IsNullOrWhiteSpace(DataMessage.Name))
            {
                var message = new { error = "��� �� ������ ���� ������" };
                return message.ToJson();
            }

            try
            {
                var configuration = new ConfigurationBuilder()
                    .AddJsonFile("DataMessage.json")
                    .Build();

               
            }
            catch (Exception ex)
            {
                DataMessage.Logger.Error($"������ �� ����� ������: {ex}");
                var message = new { error = ex.Message };
                return message.ToJson();
            }

            return null;
        }
        
        [HttpGet("[action]")]
        public IEnumerable<WeatherForecast> WeatherForecasts()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                DateFormatted = DateTime.Now.AddDays(index).ToString("d"),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            });
        }

        public class WeatherForecast
        {
            public string DateFormatted { get; set; }
            public int TemperatureC { get; set; }
            public string Summary { get; set; }

            public int TemperatureF
            {
                get
                {
                    return 32 + (int)(TemperatureC / 0.5556);
                }
            }
        }
    }
}
