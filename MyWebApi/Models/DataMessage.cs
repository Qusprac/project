﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Formatting = Newtonsoft.Json.Formatting;

namespace MyWebApi.Models
{
    public class DataMessage
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Message { get; set; }
        public static Logger Logger = LogManager.GetCurrentClassLogger();
    
    }
}
